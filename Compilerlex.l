%{
#define IDNAME 1
#define INTEGERLIT 2
#define FLOATLIT 3
#define EXFLOATLIT 4
#define STRING 5
#define COMMA 6
#define SEMICOLONSYM 7 
#define LEFTPARENTHESIS 8
#define RIGHTPARENTHESIS 9
#define BEGINSYM 10
#define END 11
#define READ 12
#define WRITE 13
#define IF 14
#define THEN 15
#define ELSE 16
#define ENDIF 17
#define FOR 18
#define TO 19
#define ENDFOR 20
#define WHILE 21
#define ENDWHILE 22
#define REPEAT 23
#define REPEATEND 24
#define DECLARE 25
#define AS 26
#define INTEGER 27
#define REAL 28
#define BOOL 29
#define SCANEOF 30
#define ASSIGN 31
#define ADD 32
#define SUB 33
#define MULT 34
#define DIV 35
#define NEQUAL 36
#define GREATTHAN 37
#define LESSTHAN 38
#define GREATEQUAL 39
#define LESSEQUAL 40
#define EQUAL 41
#define INCRE 42
#define DECRE 43
#define REMINDER 44
#define LOGICAND 45
#define LOGICOR 46
#define LOGICNOT 49
#define ADDASSIGN 50
#define SUBASSIGN 51
#define MULTASSIGN 52
#define DIVASSIGN 53
#define REMINDASSIGN 54
#define BITAND 55
#define BITOR 56
#define BITXOR 57
#define NTOKEN 58
#define NID 59
#define NINTLIT 60
#define NSTRING 61
#define NOPERATOR 62
#define NASSIGN 63
#define NCOMPRESS 64
#define NFLOAT 65
#define NEXFLOAT 66
#define COMMENTST 67
#define COMMENTED 68

unsigned linecount = 1;
%}

Word [^ \t\n]+
Digit [0-9]
NZeroDigit [1-9]
Letter [a-zA-Z]
IntLit (0|{NZeroDigit}{Digit}*)
DecimalLit (0|0*{NZeroDigit}+)
NZeroIntLit {NZeroDigit}{Digit}*
Id {Letter}({Letter}|{Digit}|_)*
FloatLit {IntLit}\.{DecimalLit}
Ex [Ee]\-*({IntLit}|{FloatLit})
ExFloatLit {FloatLit}{Ex}
End [Ee][Nn][Dd]
If [Ii][Ff]
For [Ff][Oo][Rr]
While [Ww][Hh][Ii][Ll][Ee]
Repeat [Rr][Ee][Pp][Ee][Aa][Tt]
CommentSym (\/\/|\%\%)
CommentNLineSt (\/\*|\{\%|\{\*)
CommentNLineEd (\*\/|\%\}|\*\})
CommentLine {CommentSym}.*\n

NToken .(\,\,+|\.\.+).
NId {Digit}+{Letter}+
NIntLit (0)+{Digit}+
NString \"[^\"\n]*\;
NFloat ({Digit}*{Letter}+{Digit}*\.{IntLit}|{IntLit}\.{Digit}*{Letter}+{Digit}*|\.{IntLit}|{IntLit}\.)
NEx [Ee]\-*{Digit}*{Letter}+{Digit}*
NExFloat ({FloatLit}{NEx}|{NFloat}{Ex}|{NFloat}{NEx})
NAdd (\+\-|\+\*|\+\/|\+\%)
NSub (\-\+|\-\*|\-\/|\-\%)
NMult (\*\+|\*\-|\*\*|\*\/|\*\%)
NDiv (\/\+|\/\-|\/\%)
NResi (\%\+|\%\-|\%\*|\%\/)
NOperator ({NAdd}|{NSub}|{NMult}|{NDiv}|{NResi})
NAssign (\:\=(\:\=)+|\:\=\,)
NGreatThan (\>\>+|\>\<+|\>\=(\>|\<|\=)+)
NLessThan (\<\>+|\<\<+|\<\=(\>|\<|\=)+)
NEqual (\=\>+|\=\<+|\=\=\=+)
NCompress ({NGreatThan}|{NLessThan}|{NEqual})
%%
[ \t] { }
[\n] {linecount++;}
{CommentNLineSt} {return(COMMENTST);}
{CommentNLineEd} {return(COMMENTED);}
{CommentLine} {linecount++;}

[Bb][Ee][Gg][Ii][Nn] {return(BEGINSYM);}
{End} {return(END);}
[Rr][Ee][Aa][Dd] {return(READ);}
[Ww][Rr][Ii][Tt][Ee] {return(WRITE);}
{If} {return(IF);}
[Tt][Hh][Ee][Nn] {return(THEN);}
[Ee][Ll][Ss][Ee] {return(ELSE);}
{End}{If} {return(ENDIF);}
{For} {return(FOR);}
[Tt][Oo] {return(TO);}
{End}{For} {return(ENDFOR);}
{While} {return(WHILE);}
{End}{While} {return(ENDWHILE);}
{Repeat} {return(REPEAT);}
{Repeat}{End} {return(REPEATEND);}
[Dd][Ee][Cc][Ll][Aa][Rr][Ee] {return(DECLARE);}
[Aa][Ss] {return(AS);}
[Ii][Nn][Tt][Ee][Gg][Ee][Rr] {return(INTEGER);}
[Rr][Ee][Aa][Ll] {return(REAL);}
[Bb][Oo][Oo][Li] {return(BOOL);}
[Ss][Cc][Aa][Nn][Ee][Oo][Ff] {return(SCANEOF);}

{NToken} {return(NTOKEN);}
{NId} {return(NID);}
{NIntLit} {return(NINTLIT);}
{NString} {return(NSTRING);}
{NOperator} {return(NOPERATOR);}
{NAssign} {return(NASSIGN);}
{NCompress} {return(NCOMPRESS);}
{NFloat} {return(NFLOAT);}
{NExFloat} {return(NEXFLOAT);}
{IntLit} {return(INTEGERLIT);}
{ExFloatLit} {return(EXFLOATLIT);}
{FloatLit} {return(FLOATLIT);}
{Id} {return(IDNAME);}
\"[^\"\n]*\" {return(STRING);}

"," {return(COMMA);}
";" {return(SEMICOLONSYM);}
"(" {return(LEFTPARENTHESIS);}
")" {return(RIGHTPARENTHESIS);}
":=" {return(ASSIGN);}
"+" {return(ADD);}
"-" {return(SUB);}
"*" {return(MULT);}
"/" {return(DIV);}
"!=" {return(NEQUAL);}
">" {return(GREATTHAN);}
"<" {return(LESSTHAN);}
">=" {return(GREATEQUAL);}
"<=" {return(LESSEQUAL);}
"==" {return(EQUAL);}
"++" {return(INCRE);}
"--" {return(DECRE);}
"%" {return(REMINDER);}
"&&" {return(LOGICAND);}
"||" {return(LOGICOR);}
"!" {return(LOGICNOT);}
"+=" {return(ADDASSIGN);}
"-=" {return(SUBASSIGN);}
"*=" {return(MULTASSIGN);}
"/=" {return(DIVASSIGN);}
"%=" {return(REMINDASSIGN);}
"&" {return(BITAND);}
"|" {return(BITOR);}
"^" {return(BITXOR);}
%%
void main()
{ 
    int i = yylex();
	int printword = 1;
    while (i > 0 && i < 69) 
    {
		if(i == 67)
			printword = 0;
		else if(i == 68)
			printword = 1;
		if(printword == 1)
		{
			if(i < 58)
        		printf("Token number: %d, value:\"%s\", line:%d \n",i,yytext,linecount);
			else if(i == 58)
				printf("Line %d: %s is illegal token \n",linecount,yytext);
			else if (i == 59)
				printf("Line %d: %s is illegal Id \n",linecount,yytext);
			else if(i == 60)
				printf("Line %d: %s is illegal integer \n",linecount,yytext);
			else if(i == 61)
				printf("Line %d: %s is illegal string \n",linecount,yytext);
			else if(i == 62)
				printf("Line %d: %s is illegal operator \n",linecount,yytext);
			else if(i == 63)
				printf("Line %d: %s is illegal assignment \n",linecount,yytext);
			else if(i == 64)
				printf("Line %d: %s is illegal compression \n",linecount,yytext);
			else if(i == 65)
				printf("Line %d: %s is illegal float \n",linecount,yytext);
			else if(i == 66)
				printf("Line %d: %s is illegal exponential float \n",linecount,yytext);
		}
		i = yylex();
    }
}
